const { conectarSerialPort } = require("../utils/serialport");

exports.conectarse = async (req, res, next) => {
    try {
        if (global.device) {
            if (!global.device.isOpen()) {
                await conectarSerialPort();
            }
            next();
        } else {
            await conectarSerialPort();
            next();
        }
    } catch (error) {
        console.log("error", error);
        return res.status(400).json({ err: error.message });
    }
};

exports.startTimeout = (req, res, next) => {
    res.setTimeout(5000, function () {
        console.log("Request has timed out.");
        global.device.removeAllListeners("data")
        res.status(500).json({ message: "Task timed out!", code: "1" });
    });
    next()
}
