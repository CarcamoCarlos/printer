const express = require("express");
const { imprimirInfo, obtenerEstados } = require("../controller.js");
const router = express.Router();
const { conectarse, startTimeout } = require("../middleware/");
const { upload } = require("../utils/files.js");

router.post("/print", [conectarse, upload.array('img', 10)], imprimirInfo);

router.get("/get-status", [conectarse,startTimeout], obtenerEstados);

module.exports = router;