require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.PORT
const bodyParser = require('body-parser')
const queue = require('express-queue');
const path = require("path");
const { createDirectory } = require('./utils/files')

process.env.TMP_DIRECTORY = path.resolve(__dirname) + "/tmp";

createDirectory(process.env.TMP_DIRECTORY)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(queue({ activeLimit: 1, queuedLimit: -1 }));

app.use("/",require('./routes/routes'));

app.listen(port, (err) => {
  if (err) {
    return console.log('Something wrong happened', err)
  }
  console.log(`API A200 server is listening on ${port}`)
})