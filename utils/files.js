const fs = require("fs");
const multer = require("multer")
const path = require("path");

exports.createDirectory = async (directory) => {
    try {
        await fs.promises.mkdir(directory, { recursive: true });
    } catch (error) {
        console.error(err)
    }
}

const fileFilter = (req, file, callback) => {
    var ext = path.extname(file.originalname);
    var extensions = ['.img', '.png', '.jpg']
    if(!extensions.includes(ext) ) {
        return callback(new Error('Formato Invalido. Formatos permitidos: .img, .png, .jpg'))
    }
    callback(null, true)
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './tmp/')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })

exports.emptyDir = async (directory) => {
    try {
        const files = await fs.promises.readdir(directory);
        for (const file of files)
            await fs.promises.unlink(`${directory}/${file}`)
    } catch (error) {
        console.error(err)
    }

}

exports.existsFile = (directory) => {
    return fs.existsSync(directory)
}
exports.upload = multer({ storage, fileFilter })