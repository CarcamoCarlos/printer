'use strict';
const util          = require('util');
const EventEmitter  = require('events');
const {SerialPort} = require('serialport');
const escpos = require("escpos");

/**
 * SerialPort device
 * @param {[type]} port
 */
function Serial(port){
  var self = this;
  this.device = new SerialPort({path: port, baudRate: 115200, parity:"none", stopBits:1, dataBits: 8, flowControl:false,autoOpen: false});
  this.device.on('close', function() {
    self.emit('disconnect', self.device);
  });
  EventEmitter.call(this);
  return this;
};

util.inherits(Serial, EventEmitter);

/**
 * open deivce
 * @param  {Function} callback
 * @return {[type]}
 */
Serial.prototype.open = function(callback){
  this.device.open(callback);
  return this;
};

/**
 * write data to serialport device
 * @param  {[type]}   buf      [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
Serial.prototype.write = function(data, callback){
  this.device.write(data, callback);
  return this;
};

/**
 * close device
 * @param  {Function} callback  [description]
 * @param  {int}      timeout   [allow manual timeout for emulated COM ports (bluetooth, ...)]
 * @return {[type]} [description]
 */
Serial.prototype.close = function(callback, timeout) {

  var self = this;

  this.device.drain(function() {

    self.device.flush(function(err) {

      setTimeout(function() {

        callback && callback(err, self.device)

      }, "number" === typeof timeout && 0 < timeout ? timeout : 0);

    });

  });

  return this;

};

/**
 * read buffer from the printer
 * @param  {Function} callback
 * @return {Serial}
 */
Serial.prototype.read = function(callback) {
  this.device.on('data', function(data) {
    callback(data);
  });
  return this;
};

Serial.prototype.removeAllListeners = function(nameListener) {
  this.device.removeAllListeners(nameListener)
  return this;
};

Serial.prototype.isOpen = function() {
  return this.device.isOpen
};

const buscarSerialPort = async (vendorId) => {
  const serialList = await SerialPort.list();
  const portFound = serialList.find((port) => port.vendorId === vendorId);
  console.log(portFound)
  if (!portFound) {
      throw new Error(
          "Impresora no conectada. Chequear el vendorId: 067b de la impresora"
      );
  }
  return portFound
}

const conectarSerialPort = () => {
  return new Promise(async (resolve,reject) => {
    const portFound = await buscarSerialPort('067b')
    const options = { encoding: "GB18030" /* default */ };
    global.device = new Serial(portFound.path)
    global.printer = new escpos.Printer(global.device, options);
    global.device.open((err) => {
      if(err) {
        console.log(err)
        reject(err)
      }
      resolve()
    })
  })
}

/**
 * expose
 */
module.exports = {
  conectarSerialPort,
  escpos
};