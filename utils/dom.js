const jsdom = require("jsdom");
const { existsFile } = require("./files");
const { JSDOM } = jsdom;
const {elementosValidos} = require("./elementosValidos")

function crearDOM(stringDOM) {
    const dom = new JSDOM(`<!DOCTYPE html>${stringDOM}`);
    return dom.window.document.querySelector("body");
}

function extraerAtributosDelNodo(nodo, configuracionTag) {
    let alertas = [];
    let datos = { tag: configuracionTag.tag };

    // asignamos los atributos necesarios
    configuracionTag.attributes.forEach((atr) => {
        let valueAttribute = parseValue(atr.type, nodo.getAttribute(atr.name));
        // validamos los atributos, en caso de ser errones, le asignamos el por defecto
        if (valueAttribute === null || !atr.options.includes(valueAttribute)) {
            alertas.push(
                `Elemento ${configuracionTag.tag} sin atributo. Atributo: ${atr.name}. Valor: ${valueAttribute}. Se asigno el valor por defecto: ${atr.default}. Valores disponibles: [${atr.options}]`
            );
            datos[atr.name] = atr.default;
        } else {
            datos[atr.name] = valueAttribute;
        }
    });
    // si tiene contenido como un <text>contenido</text> se lo agregamos
    if (configuracionTag.content) {
        datos.content = nodo.innerHTML;
    }

    if (
        datos.tag === "IMAGEN" &&
        (datos.content === "" ||
            !existsFile(`${process.env.TMP_DIRECTORY}/${datos.content}`))
    ) {
        alertas.push(`No se encontro la imagen: ${datos.content}`);
        return { alertas: alertas, datos: null };
    }

    return { alertas: alertas, datos };
}

function obtenerTagDelNodo(nodo) {
    return nodo.tagName;
}

function elTagEsValido(tag) {
    let elemento = elementosValidos.find(
        (e) => e.tag.toUpperCase() === tag.toUpperCase()
    );
    if (elemento === undefined) return false;
    return true;
}

function obtenerConfiguracionDelTag(tag) {
    return elementosValidos.find((e) => e.tag.toUpperCase() === tag.toUpperCase());
}
function obtenerElementosDelDOM(stringDOM) {
    let elementos = [];
    let alertas = [];
    let DOM = crearDOM(stringDOM);
    var cantidadDeHijos = DOM.childNodes.length;
    while (cantidadDeHijos > 0) {
        let primerHijo = DOM.firstElementChild;
        let tag = obtenerTagDelNodo(primerHijo);
        if (!elTagEsValido(tag)) {
            alertas.push(`Elemento invalido: ${tag}`);
            DOM.removeChild(primerHijo);
            continue;
        }
        let configuracionTag = obtenerConfiguracionDelTag(tag);
        let info = extraerAtributosDelNodo(primerHijo, configuracionTag);
        if (info.datos !== null) {
            elementos.push(info.datos);
        }
        if (info.alertas.length !== 0) {
            alertas.push(...info.alertas);
        }
        DOM.removeChild(primerHijo);
        cantidadDeHijos = DOM.childNodes.length;
    }
    return { alertas, elementos };
}

function parseValue(type, value) {
    if (value === null) {
        return null;
    } else if (type === "number") {
        return Number(value);
    } else {
        return value.toUpperCase();
    }
}

module.exports = {
    obtenerElementosDelDOM,
};
