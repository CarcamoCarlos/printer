exports.elementosValidos = [
    {
        tag: "ALIGN",
        content: false,
        attributes: [
            {
                name: "type",
                type: "string",
                default: "CT",
                options: ["CT", "LT", "RT"],
            },
        ],
    },
    {
        tag: "FONT-TYPE",
        content: false,
        attributes: [
            { name: "type", default: "A", type: "string", options: ["A", "B"] },
        ],
    },
    {
        tag: "FONT-SIZE",
        content: false,
        attributes: [
            { name: "height", default: 1, type: "number", options: [1, 2] },
            { name: "width", type: "number", default: 1, options: [1, 2] },
        ],
    },
    {
        tag: "FONT-STYLE",
        content: false,
        attributes: [
            {
                name: "type",
                type: "string",
                default: "NORMAL",
                options: ["NORMAL", "BIU2"],
            },
        ],
    },
    { tag: "TEXT", content: true, attributes: [] },
    {
        tag: "FEED",
        content: false,
        attributes: [
            {
                name: "number",
                type: "number",
                default: 1,
                options: [1, 2, 3, 4],
            },
        ],
    },
    {
        tag: "BARCODE",
        content: true,
        attributes: [
            {
                name: "type",
                type: "string",
                default: "EAN8",
                options: [
                    "EAN8",
                    "UPC_A",
                    "UPC_E",
                    "EAN13",
                    "CODE39",
                    "ITF",
                    "NW7",
                    "CODE39",
                    "CODE128",
                ],
            },
            { name: "height", default: 1 },
            { name: "width", default: 1 },
            { name: "position", default: "BLW" },
            { name: "font", default: "A" },
            { name: "includeParity", default: false },
        ],
    },
    { tag: "IMAGEN", content: true, attributes: [] },
    { tag: "QRIMAGE", content: true, attributes: [] },
];