const {escpos} = require("../utils/serialport")

function imprimir(array){
  return new Promise(async (resolve,rejected) => {
    for(const element of array){
      await asignar(element)
    }
    await terminarImpresion()
    resolve("ok")
  })
}

function loadImage(file){
  return new Promise((resolve,reject) => {
    let path = `${process.env.TMP_DIRECTORY}/${file}`
    escpos.Image.load(path, function(image){
      resolve(image)
    })
  })
}

function qrimage(text){
  return new Promise((resolve,reject) => {
    global.printer.qrimage(text, () => {
      resolve()
    })
  })
}

async function terminarImpresion(){
    await global.printer.cut()
    await global.printer.close()
}

async function asignar(element){
  switch(element.tag){
    case 'ALIGN':
      await global.printer.align(element.type);
      break;
    case 'FONT-TYPE':
      await global.printer.font(element.type)
      break;
    case 'FONT-SIZE':
      await global.printer.size(Number(element.height), Number(element.width))
      break;
    case 'FONT-STYLE':
      await global.printer.style(element.type)
      break;
    case 'TEXT':
      await global.printer.text(element.content)
      break;
    case 'FEED':
      await global.printer.feed(Number(element.number))
      break;
    case 'BARCODE':
      await global.printer.barcode(element.content, element.type)
      break;
    case 'IMAGEN':
      let image = await loadImage(element.content)
      await global.printer.image(image, 's8')
      break;
    case 'QRIMAGE':
      await qrimage(element.content);
      break;
    default:
      console.log("tag no coincide")
      break;
  }
}

function getPrinterStatus(code){
  let status;
  switch (code) {
      case 18:
          status = "OK";
          break;
      case 50:
          status = "OK";
          break;
      case 210:
          status = "feed button pressed";
          break;
      case 82:
          status = "feed button pressed";
          break;
      case 178:
          status = "Paper not detected";
          break;
      case 146:
          status = "OK";
          break;
      case 114:
          status = "Moving paper";
          break;
      default:
          status = "UNKNOWN";
          break;
  }
  return status
};

function getOfflineCauseStatus(code){
  let status;
  switch (code) {
      case 18:
          status = "OK";
          break;
      case 50:
          status = "Check paper";
          break;
      case 54:
          status = "Printer door open";
          break;
      case 22:
          status = "Printer door open";
          break;
      case 26:
          status = "Moving paper";
          break;
      default:
          status = "UNKNOWN";
          break;
  }
  return status
};

function getErrorCauseStatus(code){
  let status;
  switch (code) {
      case 18:
          status = "OK";
          break;
      case 26:
          status = "Warning";
          break;
      case 50:
          status = "Critical error";
          break;
      case 58:
          status = "Critical error";
          break;
      default:
          status = "UNKNOWN";
          break;
  }
  return status
};

function getRollPaperSensorStatus(code){
  let status;
  switch (code) {
    case 18:
      status = "OK";
      break;
    case 30:
        status = "Low paper level";
        break;
    case 114:
        status = "Paper not detected in the door printer";
        break;
    case 126:
        status = "Out of paper";
        break;
    default:
        status = "UNKNOWN ";
        break;
  }
  return status
};

async function getStatus(command, functionDecodeStatus){
  const code = await escribirSerialPort(command)
  const status = {
    status: functionDecodeStatus(code),
    code: code
  }
  return status
}

function getStatuses(){
  return new Promise(async (resolve,rejected) => {
    let statuses
    statuses = {
      printerStatus: await getStatus('\x10\x04\x01', getPrinterStatus),
      offlineCauseStatus:await getStatus('\x10\x04\x02', getOfflineCauseStatus),
      errorCauseStatus:await getStatus('\x10\x04\x03', getErrorCauseStatus),
      rollPaperSensorStatus:await getStatus('\x10\x04\x04', getRollPaperSensorStatus)
    }
    resolve(statuses)
  })
}

const escribirSerialPort = (command) => {
  return new Promise((resolve, rejected) => {
    global.device.write(command)
    global.device.read(function (data) {
        data = Buffer.from(data, "hex");
        global.device.removeAllListeners("data");
        resolve(data[0])
    });
  })
}; 

module.exports = {
  imprimir,
  getStatus,
  getStatuses
}