const { obtenerElementosDelDOM } = require("../utils/dom");
const { emptyDir } = require("../utils/files");
const { imprimir, getStatuses } = require("../utils/printer");


exports.obtenerEstados = async (req,res) => {
    try {
        let resultado = await getStatuses()

        return res.json({...resultado})  
    } catch (error) {
        console.log(error)
        return res.status(400).json({msg:error})
    }
}


exports.imprimirInfo = async (req,res) => {
    try {
        let body = req.body.body;
        if(!body){
            return res.status(400).json({msg:"DOM invalido", alertas: array.alertas})
        }
        body = body.replaceAll("\n", "")
        // var body = `<align type="ct"></align><font-type type="a"></font-type><font-style type="bu2"></font-style><font-size height="2" width="2"></font-size><text>sabes que si</text><text>asdsad asdasd asdasdd</text><text>asdsad asdasd asdasdd</text><text>asdsad asdasd asdasdd</text><img></img><qrimage>www.google.com.ar</qrimage><feed number="4"></feed>`
        let array = obtenerElementosDelDOM(body);
        if(array.elementos.length === 0){
            return res.status(400).json({msg:"DOM invalido", alertas: array.alertas})
        }
        await imprimir(array.elementos)
        emptyDir(process.env.TMP_DIRECTORY)
        return res.json({msg:"ok", alertas:array.alertas})  
    } catch (error) {
        console.log(error)
        return res.status(400).json({msg:error})
    }
}